from django.db import models


class Author(models.Model):
    name = models.CharField(
        verbose_name = 'ФИО',
        max_length = 150
    )
    birth_date = models.DateField(verbose_name = 'Дата рождения')

    country = models.CharField(
    	verbose_name = 'Страна',
    	max_length = 70
    	)
    def __str__(self):
        return self.name

class Genre(models.Model):
    genre_title = models.CharField(
        verbose_name = 'Жанр',
        max_length = 100
    )
    def __str__(self):
        return self.genre_title


class Book(models.Model):
    title = models.CharField(
        verbose_name = 'Название книги',
        max_length = 100
    )
    about_book = models.TextField(
        verbose_name = 'О книге'
    )

    pages = models.PositiveSmallIntegerField(
            verbose_name = 'Кол-во страниц'
        )
    year = models.DateField(verbose_name='Год выхода')
    author = models.ForeignKey('Author', on_delete=models.CASCADE)
    genre = models.ForeignKey('Genre', on_delete=models.CASCADE)

    def __str__(self):
        return self.title






